# Collection of custom tooling for Carbon Black products

*NOTE: I do not work for nor represent [Carbon Black](https://www.carbonblack.com)*.

## cbd-single-system-events.py
Python script for Carbon Black Defense which pulls events out of the back-end for a single system via the Carbon Black Defense API. Events are piped to a plain-text output file for analysis.

What makes this somewhat unique is that the CbD API only lets you pull two week's worth of data per query. By configuring the `week_calc()` function you can extract events going back months or years depending on the CbD back-end retention.

**Usage**:

Add your API token, API URL, and the system hostname you wish to pull events for. Edit the `week_calc()` function input as necessary to dictate time frame to pull events for.

